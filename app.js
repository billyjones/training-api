var express = require('express');
var mongoose = require('mongoose');
var expressJwt = require('express-jwt');
var jwt = require('jsonwebtoken');

var app = express();

var secret = "A92LWsdBgH6legaUm8U3uyJ7n1bdEik7WvO8nQab9LlHTtnawpRx8d-HPqW0b2g-";

app.use('/api', expressJwt({secret: secret}));

// app.use(express.json());
// app.use(express.urlencoded());
app.use(express.bodyParser());

var cors = require('cors');
app.use(cors());
app.use(app.router);	

var Schema = mongoose.Schema;
var workoutSchema, Workout, UserSchema, User;

// default to a 'localhost' configuration:
var connection_string = 'localhost/training_plus';
// if OPENSHIFT env variables are present, use the available connection info:
if(process.env.OPENSHIFT_MONGODB_DB_PASSWORD){
  connection_string = process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +
  process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +
  process.env.OPENSHIFT_MONGODB_DB_HOST + ':' +
  process.env.OPENSHIFT_MONGODB_DB_PORT + '/' +
  process.env.OPENSHIFT_APP_NAME;
}

mongoose.connect('mongodb://'+connection_string);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {});

// workouts
workoutSchema = mongoose.Schema({
    label: String,
	rest_between_exercises: Number,
	rest_between_sets: Number,
	repeat: Number,
	exercises: [],
	created_at: Date,
	updated_at: Date,
	user: { type: Schema.Types.ObjectId, ref: 'users' },
	public: Boolean
});

Workout = mongoose.model('workouts', workoutSchema);

UserSchema = mongoose.Schema({
    username: String,
    first_name: String,
    last_name: String,
	email: String,
	password: String,
	created_at: Date,
	updated_at: Date,
	avatar_url: String
});

User = mongoose.model('users', UserSchema);

// authenitcation

app.post('/authenticate', function (req, res) {
  //TODO validate req.body.username and req.body.password
  //if is invalid, return 401
  if (!(req.body.username === 'john.doe' && req.body.password === 'foobar')) {
    res.send(401, 'Wrong user or password');
    return;
  }

  var profile = {
    first_name: 'John',
    last_name: 'Doe',
    email: 'john@doe.com',
    id: 123
  };

  // We are sending the profile inside the token
  var token = jwt.sign(profile, secret, { expiresInMinutes: 60*5 });

  res.json({ token: token });
});

app.get('/api/restricted', function (req, res) {
  console.log('user ' + req.user.email + ' is calling /api/restricted');
  res.json({
    name: 'foo'
  });
});

app.get('/users', function(req, res){
	
	var limit = typeof req.query.limit !== 'undefined' ? req.query.limit : 0;
	var offset = typeof req.query.offset !== 'undefined' ? req.query.offset : 0;

	var query = {};

	User.find(query).limit(limit).skip(offset).exec(function(err, data){
		if (err) return handleError(err);
		res.json(data);
	});

});

app.post('/users', function(req, res){

	var user;
	user = new User({
		username: req.body.username,
		first_name: req.body.first_name,
		last_name: req.body.last_name,
		email: req.body.email,
		password: req.body.password,
		avatar_url: req.body.avatar_url
	});

	user.save(function (err) {
		if (err) return handleError(err);
	});

	return res.send(user);

});

app.put('/users/:id', function(req, res){

	var data = {
		username: req.body.username,
		first_name: req.body.first_name,
		last_name: req.body.last_name,
		email: req.body.email,
	}

	User.findByIdAndUpdate(req.params.id, data, null, function(user){
		return res.send(user);
	});

});

app.get('/workouts', function(req, res){
	
	var limit = typeof req.query.limit !== 'undefined' ? req.query.limit : 0;
	var offset = typeof req.query.offset !== 'undefined' ? req.query.offset : 0;
	var user_id = typeof req.query.user_id !== 'undefined' ? req.query.user_id : null;
	var public = typeof req.query.public !== 'undefined' ? req.query.public : null;

	query = {};

	if(user_id !== null) {
		query.user = user_id;
	}

	if(public !== null) {
		query.public = public;
	}

	Workout.find(query).populate('user').limit(limit).skip(offset).exec(function(err, data){
		if (err) return handleError(err);
		res.json(data);
	});

});

app.get('/workouts/:id', function(req, res){

	Workout.findById(req.params.id, function(err, data){
		if (err) return handleError(err);
		res.json(data);
	});

});

app.post('/workouts', function(req, res){

	var workout;
	workout = new Workout({
		label: req.body.label,
		rest_between_exercises: req.body.rest_between_exercises,
		rest_between_sets: req.body.rest_between_sets,
		repeat: req.body.repeat,
		exercises: req.body.exercises,
		created_at: new Date(),
		updated_at: new Date()
	});

	workout.save(function (err) {
		if (err) return handleError(err);
	});

	return res.send(workout);

});

app.put('/workouts/:id', function(req, res){

	var data = {
		exercises: (typeof req.body.exercises !== 'undefined' ? req.body.exercises : []),
		label: req.body.label,
		rest_between_exercises: req.body.rest_between_exercises,
		rest_between_sets: req.body.rest_between_sets,
		repeat: req.body.repeat,
		updated_at: new Date()
	}

	Workout.findByIdAndUpdate(req.params.id, data, null, function(workout){
		return res.send(workout);
	});

});

app.put('/workouts/:id/exercises', function(req, res){

	var workout;

	var exercises = req.body.exercises;

	Workout.findByIdAndUpdate(req.params.id, {exercises: exercises}, null, function(workout){
		return res.send(workout);
	});

});

app.delete('/workouts/:id', function(req, res){

	Workout.findById(req.params.id, function(err, workout){
		if (err) return handleError(err);
		workout.remove();
	});

});

var handleError = function(err){
	console.log(err);
	res.json({error: err}, 500);
	return;
}

app.listen(3000);